<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;

class SendMenuToUsersCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:send-menu-to-users';

    private $today;

    private $filesystem;

    private $connection;

    protected function configure()
    {
        $today = new \DateTime();
        $this->today = $today->format('Y-m-d');
        $this->filesystem = new Filesystem();
        $this->connectionParams = array(
            'dbname' => '%env(string:DB_DBNAME)%',
            'user' => '%env(string:DB_USER)%',
            'password' => '%env(string:DB_PASSWORD)%',
            'host' => '%env(string:DB_HOST)%',
            'driver' => 'pdo_pgsql',
        );
        $this->connection = DriverManager::getConnection($this->connectionParams);
    }

    /**
     * Main command execute method.
     * @return Symfony\Component\Console\Command\Command Command result
     * @throws
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->create_cache_folder();
        $subject = 'Rozesílka menu z vybraných restaurací od Zomato';
        $menu_members = $this->get_members();
        foreach ($menu_members as $member) {
            $member_restaurants = $this->get_member_restaurants($member['id']);
            $body = 'Dobrý den,\nv přílohách můžete nalézt dnešní menu z vybraných restaurací.';
            $body_html = '<p>Dobrý den,<br />v přílohách můžete nalézt dnešní menu z vybraných restaurací.</p>';
            if (! $this->send_email($member['member_email'], $subject, $body, $body_html)) {
                $this->remove_cache_folder();
                return Command::FAILURE;
            }
        }

        $this->remove_cache_folder();
        return Command::SUCCESS;
    }

    /**
     * @return
     * @throws
     */
    private function create_cache_folder()
    {
        try {
            $this->filesystem->mkdir(sys_get_temp_dir().'/'.$this->today);
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
    }

    /**
     * @return
     * @throws
     */
    private function remove_cache_folder()
    {
        try {
            $this->filesystem->remove(sys_get_temp_dir().'/'.$this->today);
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while removing your directory at ".$exception->getPath();
        }
    }

    /**
     * @return array List of members
     * @throws
     */
    private function get_members(): ?array
    {
        return $this->connection->fetchAll('SELECT * FROM menu_members');
    }

    /**
     * @return array List of member restaurants
     * @throws
     */
    private function get_member_restaurants(int $member_id): ?array
    {
        $stmt = $this->connection->prepare('SELECT * FROM menu_member_restaurants WHERE member_id = :member_id');
        $stmt->bindParam(':member_id', $member_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return array Restaurant daily menu list
     * @throws Exception
     */
    private function get_restaurant_daily_menu(int $restaurant_id): ?array
    {
        $daily_menu = $this->get_restaurant_daily_menu_from_cache($restaurant_id);
        if (! $daily_menu) {
            $daily_menu = $this->get_restaurant_daily_menu_from_zomato($restaurant_id);
            if (! $daily_menu) {
                throw new \Exception("An error occurred while downloading daily menu");
            }
            $this->set_restaurant_daily_menu_cache($restaurant_id, $daily_menu);
        }
        return $daily_menu;
    }

    /**
     * @return null|array Restaurant daily menu list
     * @throws
     */
    private function get_restaurant_daily_menu_from_cache(int $restaurant_id): ?array
    {
        $file_path = sys_get_temp_dir().'/'.$this->today.'/'.$restaurant_id;
        try {
            if (! $this->filesystem->exists($file_path)) {
                return null;
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }

        return json_decode(file_get_contents($file_path));
    }

    /**
     * @return null|array Restaurant daily menu list
     * @throws
     */
    private function get_restaurant_daily_menu_from_zomato(int $restaurant_id): ?array
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://developers.zomato.com/geocode', [
            'query' => [
                'user-key' => '%env(string:ZOMATO_API_KEY)%',
                'res_id' => $restaurant_id,
            ],
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            return null;
        }
        $content = $response->toArray();
        return $content['daily_menu'];
    }

    /**
     * @return
     * @throws
     */
    private function set_restaurant_daily_menu_cache(
        int $restaurant_id, string $daily_menu): ?array
    {
        $file_path = sys_get_temp_dir().'/'.$this->today.'/'.$restaurant_id;
        try {
            $this->filesystem->dumpFile($file_path, json_encode($daily_menu));
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
    }

    /**
     * Email sender.
     * @return bool Result
     * @throws
     */
    private function send_email(string $address, string $subject, string $body, string $body_html,
        array $restaurant_ids, MailerInterface $mailer): ?bool
    {
        $email = (new Email())
            ->from('info@example.com')
            ->to($address)
            ->bcc('log@example.com')
            ->replyTo('unsubscribe@example.com')
            ->subject($subject)
            ->text($body)
            ->html($body_html);

        foreach ($restaurant_ids as $restaurant_id) {
            $file_path = sys_get_temp_dir().'/'.$this->today;
            $email->attachFromPath($file_path);
        }

        return $mailer->send($email);
    }
}
