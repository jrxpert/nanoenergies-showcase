<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\DBAL\DriverManager;

class DefaultController extends AbstractController
{
    public function __construct()
    {
        $this->connectionParams = array(
            'dbname' => '%env(string:DB_DBNAME)%',
            'user' => '%env(string:DB_USER)%',
            'password' => '%env(string:DB_PASSWORD)%',
            'host' => '%env(string:DB_HOST)%',
            'driver' => 'pdo_pgsql',
        );
    }

    /**
     * @Route("/{reactRouting}", name="home", defaults={"reactRouting": null})
     */
    public function index()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/api/restaurants", name="restaurants", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws
     */
    public function getRestaurants()
    {
        $restaurants = $this->get_restaurants();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($restaurants));
        return $response;
    }

    /**
     * @Route("/api/subscribe", name="subscribe", methods={"POST"})
     * @return bool Result of member save
     * @throws
     */
    public function save_member(): ?bool
    {
        $request = Request::createFromGlobals();
        $restaurant_id = $request->query->get('restaurant_id');
        $member_email = $request->query->get('member_email');
        $connection = DriverManager::getConnection($this->connectionParams);
        $stmt_select = $connection->prepare('SELECT * FROM menu_member WHERE member_email = :member_email');
        $stmt_select->bindParam(':member_email', $member_email, PDO::PARAM_STRING);
        $stmt_select->execute();
        $member = $stmt_select->fetch();
        if ($member) {
            return $this->save_member_restaurant_connection($member['id'], $restaurant_id);
        }
        $stmt_insert = $connection->prepare('INSERT INTO menu_member (member_email) VALUES (:member_email)');
        $stmt_insert->bindParam(':member_email', $member_email, PDO::PARAM_INT);
        $result = $stmt_insert->execute();
        $member = $stmt_select->execute();
        return $result && $this->save_member_restaurant_connection($member['id'], $restaurant_id);
    }

    /**
     * @Route("/api/unsubscribe/{member_id<\d+>}", name="unsubscribe", methods={"GET"})
     * @return bool Result of member delete
     * @throws
     */
    public function delete_member(int $member_id): ?bool
    {
        $connection = DriverManager::getConnection($this->connectionParams);
        $stmt = $connection->prepare('DELETE FROM menu_member WHERE member_id = :member_id');
        // NOTE: menu_member_restaurant rows are deleted by CASCADE
        $stmt->bindParam(':member_id', $member_id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    /**
     * Get list of restaurants from Zomato depending on geolocation.
     * @return null|array List of restaurants
     * @throws
     */
    private function get_restaurants(): ?array
    {
        $latlon = $this->get_user_latlon();
        if (! $latlon) {
            return false;
        }
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://developers.zomato.com/geocode', [
            'query' => [
                'user-key' => '%env(string:ZOMATO_API_KEY)%',
                'lat' => $latlon['lat'],
                'lon' => $latlon['lng'],
            ],
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            return null;
        }
        $content = $response->toArray();
        return $content['nearby_restaurants'];
    }

    /**
     * Getter of user position latitude and longitude.
     * @return null|array List of restaurants
     * @throws
     */
    private function get_user_latlon(): ?array
    {
        $client = HttpClient::create();
        $response = $client->request(
            'GET',
            'https://www.googleapis.com/geolocation/v1/geolocate?key=%env(string:GOOGLE_API_KEY)%');
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            return null;
        }
        $content = $response->toArray();
        return $content['location'];
    }

    /**
     * Save member restaurant connection in database.
     * @return bool Result of member restaurant connection save
     * @throws
     */
    private function save_member_restaurant_connection(int $member_id, int $restaurant_id): ?bool
    {
        $connection = DriverManager::getConnection($this->connectionParams);
        $stmt = $connection->prepare(
    'INSERT INTO menu_member_restaurants (member_id, restaurant_id) 
                VALUES (:member_id, :restaurant_id)');
        $stmt->bindParam(':member_id', $member_id, PDO::PARAM_INT);
        $stmt->bindParam(':restaurant_id', $restaurant_id, PDO::PARAM_INT);
        return $stmt->execute();
    }
}
