import React, {Component} from 'react';
import axios from "axios";

class SignIn extends Component {
    constructor() {
        super();
        this.state = {
            step: 1,
            restaurants: [],
            restaurantId: 0,
            memberEmail: '',
            hasError: false,
            errorMessage: ''
        };
        this.selectRestaurant = this.selectRestaurant.bind(this);
        this.saveMember = this.saveMember.bind(this);
    }

    componentDidMount() {
        if (this.state.step == 1) {
            this.getRestaurants();
        }
    }

    getRestaurants() {
        axios.get(`http://localhost:8000/api/restaurants`).then(restaurants => {
            this.setState({ restaurants: restaurants.data})
        })
        console.log('getRestaurants');
    }

    selectRestaurant() {
        var e = document.getElementById('restaurant_id');
        this.setState({step: 2, restaurantId: e.options[e.selectedIndex].value});
        console.log('selectRestaurant');
    }

    saveMember() {
        this.setState({memberEmail: document.getElementById('member_email').value});
        var restaurantId = this.state.restaurantId;
        var memberEmail = this.state.memberEmail;
        var url = `http://localhost:8000/api/subscribe`
        axios.post(url, {
            restaurant_id: restaurantId,
            member_email: memberEmail
        }).then(memberEmail => {
            this.setState( { step: 3})
        })
        console.log('saveMember');
    }

    render() {
        const loading = this.state.loading;

        if (this.state.hasError) {
            return <h1>{ this.state.errorMessage }</h1>;
        }

        switch (this.state.step) {
            case 1:
                return (
                    <div>
                        <h1>Vyberte restauraci ({ this.state.step })</h1>
                        <form>
                            <select id="restaurant_id">
                                <option value="1">Default</option>
                            { this.state.restaurants.map(restaurant =>
                                <option value={restaurant.id}>{restaurant.name}</option>
                            )}
                            </select>
                        </form>
                        <button onClick={this.selectRestaurant}>Odeslat</button>
                        <div>
                            <p>
                                Pokud se chcete přihlásit k odběru denního menu z restaurace registrované na Zomato,
                                nezapomeňte v prohlížeči povolit geolokaci.
                            </p>
                        </div>
                    </div>
                )
            case 2:
                return (
                    <div>
                        <h1>Zadejte e-mail ({ this.state.step })</h1>
                        <form>
                            <input type="text" id="member_email" size="30" value="a@b.cz" />
                        </form>
                        <button onClick={this.saveMember}>Odeslat</button>
                    </div>
                )
            case 3:
                return <h1>Přihlášení dopadlo úspěšně ({ this.state.step })</h1>;
        }
    }
}

export default SignIn;