import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import '../css/app.css';
import SignIn from './components/SignIn';

ReactDOM.render(<Router><SignIn /></Router>, document.getElementById('root'));
