--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: menu_member_restaurant; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE menu_member_restaurant (
    member_id integer NOT NULL,
    restaurant_id integer NOT NULL
);


ALTER TABLE menu_member_restaurant OWNER TO postgres;

--
-- Name: seq_menu_members; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seq_menu_members
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_menu_members OWNER TO postgres;

--
-- Name: menu_members; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE menu_members (
    id integer DEFAULT nextval('seq_menu_members'::regclass) NOT NULL,
    member_email character varying(255) NOT NULL
);


ALTER TABLE menu_members OWNER TO postgres;

--
-- Data for Name: menu_member_restaurant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menu_member_restaurant (member_id, restaurant_id) FROM stdin;
\.


--
-- Data for Name: menu_members; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menu_members (id, member_email) FROM stdin;
\.


--
-- Name: seq_menu_members; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seq_menu_members', 1, false);


--
-- Name: menu_members_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY menu_members
    ADD CONSTRAINT menu_members_pk PRIMARY KEY (id);


--
-- Name: menu_members_member_email_uindex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX menu_members_member_email_uindex ON menu_members USING btree (member_email);


--
-- Name: menu_member_restaurant_member_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menu_member_restaurant
    ADD CONSTRAINT menu_member_restaurant_member_id_fk FOREIGN KEY (member_id) REFERENCES menu_members(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

